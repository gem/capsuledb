import os
import hashlib
import getpass

user = raw_input( "enter a user name: ")
passwd = False
confirm_passwd = None
while( passwd != confirm_passwd ):
    passwd = getpass.getpass( "enter a password: ")
    confirm_passwd = getpass.getpass( "confirm password: ")
    if passwd != confirm_passwd:
        print "passwords does not match"

h=hashlib.sha1()
h.update( passwd )
salt = os.urandom(16).encode('hex')
h.update( salt )
print "\n##################"
print "new user = ", user
print "sha1 encrypted password = ", h.hexdigest()
print "salt = ", salt
print "##################"

