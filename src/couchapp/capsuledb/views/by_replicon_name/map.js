function(doc) {
    emit(doc.replicon.name, {
    	"id" : doc._id,
    	"code" : doc.code ,
    	"name" : doc.replicon.name ,
    	"predicted_system" : doc.predicted_system,
    	"taxid" : doc.replicon.taxid,
    	"ncbi_id": doc.replicon.ncbi_id,
    	"strain" : doc.replicon.strain ,
    	"taxonomy" : doc.replicon.taxonomy.join(", ") ,
    	"type" : doc.replicon.type
    });
  }