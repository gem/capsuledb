function(doc) {
	taxonomy =  doc.replicon.taxonomy;
	for(var j = 0; j < taxonomy.length; j++){
		emit(taxonomy[j], {
	    	"id" : doc._id,
	    	"code" : doc.code ,
	    	"name" : doc.replicon.name ,
	    	"predicted_system" : doc.predicted_system,
	    	"taxid" : doc.replicon.taxid,
	    	"ncbi_id": doc.replicon.ncbi_id,
	    	"strain" : doc.replicon.strain ,
	    	"taxonomy" : doc.replicon.taxonomy.join( ", ") ,
	    	"type" : doc.replicon.type
	    });
	}
}


