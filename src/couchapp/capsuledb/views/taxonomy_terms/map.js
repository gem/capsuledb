function(doc) {
	taxonomy =  doc.replicon.taxonomy;
	for(var j = 0; j < taxonomy.length; j++){
		var term = taxonomy[j];
		for( var syllabe_l = 3; syllabe_l <= term.length; syllabe_l++){
			for(var i=0; i <= term.length-syllabe_l; i++){
				var key = term.substring(i, i + syllabe_l);
				key = key.toLowerCase();
				emit([key, term], 1);
			}
		}
	}
}
