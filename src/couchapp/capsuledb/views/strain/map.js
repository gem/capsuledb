function(doc) {
	var strain =  doc.replicon.strain.toLowerCase();
	for(var wl = 3 ; wl <= strain.length; wl++){
		for(var i = 0; i<= strain.length-wl; i++ ){
			emit( strain.substring(i, i+wl), doc.replicon.strain);
		}
	}
}
