	
function(newDoc, oldDoc, userCtx, secObj) {
	 if(!userCtx || userCtx.roles.indexOf("_admin") == -1 ) {
         throw({forbidden: "Bad user userCtx.name="+userCtx.name+" , userCtx.roles="+userCtx.roles});
      } else {
    	  if (!newDoc.code) throw({forbidden : "System must have a code"});
    	  if (!newDoc.genes) throw({forbidden : "System must have a genes"});
    	  if (!newDoc.predicted_system) throw({forbidden : "System must have a predicted system"});
    	  if (!newDoc.system_status) throw({forbidden : "System must have a system_status"});
    	  if (!newDoc.replicon) throw({forbidden : "System must have a replicon"});
    	  if (!newDoc.replicon.name) throw({forbidden : "Replicon must have a name"});
    	  if (!newDoc.replicon.ncbi_id) throw({forbidden : "Replicon must have a ncbi_id"});
    	  if (!newDoc.replicon.taxid) throw({forbidden : "Replicon must have a taxid"});
    	  if (!newDoc.replicon.strain) throw({forbidden : "Replicon must have a strain"});
    	  if (!newDoc.replicon.taxonomy) throw({forbidden : "Replicon must have a taxonomy"});
    	  if (!newDoc.replicon.type) throw({forbidden : "Replicon must have a type"});
      }
}

