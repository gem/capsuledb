function(head, req) {
	// !json templates.replicon
	var Mustache = require("vendor/couchapp/lib/mustache");
	var path = require("vendor/couchapp/lib/path").init(req);
	var limit = (req.query.limit)? req.query.limit -1: 0;
	var curr_page = (req.query.curr_page)? parseInt(req.query.curr_page) : 0 ;
	var query_type = (req.query.query_type)? req.query.query_type: "is";		
	var query_key = req.query.key;
	var view_name = req.path.pop();
	
	provides("html", function() {
		var first_key, last_key ;
		var first_docid, last_docid;
		var row, system;
		send(Mustache.to_html(templates.replicon.head , {}));
		var item = 0
		var more_results = false;
		var next_key= null;
		var next_docid = null;
		
		while (row = getRow()) {
			if (!first_key){ first_key = row.key };
			if (!first_docid){ first_docid = row.id };
			item++;
			if( item > limit ){
				more_results = true;
				next_key= row.key;
				next_docid = row.id;	
				break;
			}
			last_key = row.key;
			last_docid = row.id;
			system = row.value ;
			send( Mustache.to_html(templates.replicon.body , {
					"url" : path.show("system" , system.id ) ,
					"code" : system.code ,
					"predicted_system" : system.predicted_system,
					"name" : system.name,
					"taxid" : system.taxid,
					"ncbi_id" : system.ncbi_id,
					"strain" : system.strain,
					"taxonomy" : system.taxonomy,
					"type" : system.type
			}));
		}
		if (!first_key){
	        send( 'No results found');	
    	}
		
		var pagination = {};
		if( limit ){
			if( query_type =="is"){
				if( !req.query.descending ){
					if (more_results){
						pagination.next = path.list( "index" , view_name ,{ key: query_key,
							                                                startkey:next_key, 
							                                                startkey_docid: next_docid, 
							                                                limit: limit +1, 
							                                                curr_page: curr_page+1 
							                                              });
					}
					if(curr_page > 0 ){
						pagination.prev = path.list( "index" , view_name ,{ key:query_key,
																			startkey:first_key, 
							                                                startkey_docid: first_docid, 
							                                                limit: limit +1, 
							                                                skip:1, 
							                                                descending:true, 
							                                                curr_page: curr_page-1 
							                                              });
					}
				}else{
					// si je descend il ya forecement un next
					pagination.next = path.list( "index" , view_name ,{ key: query_key,
						                                                startkey:first_key, 
                                                                        startkey_docid: first_docid, 
                                                                        limit: limit +1, 
                                                                        skip:1, 
                                                                        curr_page: curr_page+1
                                                                      });
					
					if (more_results){
						pagination.prev = path.list( "index" , view_name ,{ key: query_key,
							                                                startkey: next_key, 
							                                                startkey_docid: next_docid, 
							                                                limit: limit +1, 
							                                                descending:true, 
							                                                curr_page: curr_page-1 
							                                               });	
					}
				}
			} else if ( query_type == "start_with"){
				var storeendkey= (req.query.storeendkey)? req.query.storeendkey: req.query.endkey ;
				var storestartkey= (req.query.storestartkey)? req.query.storestartkey: '"'+first_key+'"' ;
				if( !req.query.descending ){
					if(more_results){
						pagination.next = path.list( "index" ,view_name ,{ key:query_key,
					                                                       startkey:next_key, 
					                                                       startkey_docid: next_docid, 
					                                                       endkey: storeendkey ,
					                                                       limit: limit +1, 
					                                                       curr_page: curr_page+1, 
					                                                       query_type:"start_with",
					                                                       storeendkey: storeendkey,
					                                                       storestartkey: storestartkey
					                                                       });
					}
					if(curr_page > 0){
						pagination.prev =path.list( "index" ,view_name ,{key:query_key,
							                                             startkey:first_key, 
                                                                         startkey_docid: first_docid, 
                                                                         endkey: storestartkey, 
                                                                         limit: limit +1, 
                                                                         skip:1, 
                                                                         descending:true, 
                                                                         curr_page: curr_page-1, 
                                                                         query_type:"start_with",
                                                                         storeendkey: storeendkey ,
                                                                         storestartkey: storestartkey
						});
					}
				}else{
					// je descend
					if (more_results){
						pagination.prev = path.list( "index" ,view_name ,{ key:query_key,
							                                               startkey: next_key, 
							                                               startkey_docid: next_docid, 
							                                               end_key: storestartkey,
							                                               limit: limit +1, 
							                                               descending:true, 
							                                               curr_page: curr_page-1, 
							                                               query_type:"start_with",
							                                               storestartkey: storestartkey,
							                                               storeendkey: storeendkey,
						});
					
					}
					// si je descend il ya forecement un next
					pagination.next = path.list( "index" ,view_name ,{ key:query_key,
						                                               startkey:first_key, 
						                                               startkey_docid: first_docid, 
						                                               endkey: storeendkey,
						                                               limit: limit +1, 
						                                               skip:1, 
						                                               curr_page: curr_page+1, 
						                                               query_type:"start_with", 
						                                               storestartkey: storestartkey,
						                                               storeendkey: storeendkey,
					});
				}
			}
		}
		return(Mustache.to_html(templates.replicon.tail , pagination));
	});
}
	